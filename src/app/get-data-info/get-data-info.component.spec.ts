import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetDataInfoComponent } from './get-data-info.component';

describe('GetDataInfoComponent', () => {
  let component: GetDataInfoComponent;
  let fixture: ComponentFixture<GetDataInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetDataInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetDataInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
