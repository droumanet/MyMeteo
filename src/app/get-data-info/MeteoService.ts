import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Observer } from 'rxjs';
import { IMeteo} from './MeteoModel';


@Injectable()
export class MeteoService {
  MeteoValues: Observable<IMeteo>;
  private data: Observable<IMeteo>;
  private dataObserver: Observer<IMeteo>;

  constructor(private myHttp: HttpClient) {
    this.data = new Observable(observer => this.dataObserver = observer);
  }

  getMeteoJson(urlRequest): Observable<IMeteo> {
    this.MeteoValues = this.myHttp.get<IMeteo>(urlRequest);
    return this.MeteoValues;
  }
}
