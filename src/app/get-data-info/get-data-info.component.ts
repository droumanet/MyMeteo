import { Component, OnInit, Input } from '@angular/core';
import { VERSION } from '../../../node_modules/@angular/platform-browser-dynamic';
import { MeteoService } from './MeteoService';
import { Observable } from 'rxjs';
import { IMeteo } from './MeteoModel';

@Component({
  selector: 'app-get-data-info',
  templateUrl: './get-data-info.component.html',
  styleUrls: ['./get-data-info.component.css']
})

export class GetDataInfoComponent implements OnInit {
  site = 'https://www.prevision-meteo.ch/services/json/';
  // url  = 'lat=46.259lng=5.235';
  url = 'moirans';
  request = this.site + this.url;
  name: String = `www.prevision-meteo.ch (Angular ${VERSION.full})`;
  meteoResponse: any;
  prevision: Observable<IMeteo>;
  allDatas: IMeteo;
  ville = '(vide)';
  Temp: number;

  constructor(private mesMeteo: MeteoService) {
    console.log('Just Starting...');
    this.meteoResponse = this.mesMeteo.getMeteoJson(this.request)
    .subscribe(
      response => {
        this.ville = response.city_info.name;
        this.Temp = response.current_condition.tmp;
        console.log('il fait ' + this.Temp + '°C à ' + this.ville + '.');
        console.log('demain il fera : ' + response.fcst_day_0.condition);
        console.log('après-demain   : ' + response.fcst_day_1.condition);
        console.log('dans 3 jours   : ' + response.fcst_day_2.condition);
        console.log('dans 4 jours   : ' + response.fcst_day_3.condition);
        console.log('dans 5 jours   : ' + response.fcst_day_4.condition);
        console.log('Plus de détails: ' + JSON.stringify(response));
        this.allDatas = response;
      },
      function (error) {
        console.log('error: ' + error);
      },
      function () {
        console.log('et c\'est tout !');
      }
    );
    function get_forecastJ0() {
      console.log('Forecast J0 = ' + this.response.fcst_day_0.condition);
      return this.response.fcst_day_0.condition;
    }
  }

  ngOnInit() {
  }
}
