import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClientJsonpModule } from '@angular/common/http';
import {MeteoService} from './get-data-info/MeteoService';
import { AppComponent } from './app.component';
import { GetDataInfoComponent } from './get-data-info/get-data-info.component';

@NgModule({
  declarations: [
    AppComponent,
    GetDataInfoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  providers: [MeteoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
